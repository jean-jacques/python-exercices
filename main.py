from bloc_note_sauvegarde import Note

notes1 = Note()

# CREATION D'UNE NOTE
creation = input("Voulez-vous créer une note (oui/non) ?")
while (creation == "" or creation.isspace()):
    print("Entrée non valide")
    creation = input("Voulez-vous créer une note (oui/non) ?")

    # CREATION DE LA CLEF
while (creation == "oui"):
    nom = input("Entrer un nom de note: ")
    while (nom == "" or nom.isspace()):
        print("Le nom ne peut pas être vide")
        nom = input("Entrer un nom de note: ")

    # CREATION DE LA VALEUR
    contenu = input("Entrer le texte de la note: ")
    while (contenu == "" or contenu.isspace()):
        print("La note ne peut pas être vide")
        contenu = input("Entrer le texte de la note: ")
    notes1.add(nom, contenu)
    creation = input("Voulez-vous créer une autre note (oui/non) ?")
    
# RECHERCHE D'UNE NOTE DANS LE DICTIONNAIRE
rech_note = input("Voulez-vous rechercher une note (oui/non) ?")
while (rech_note == "" or rech_note.isspace()):
    print("Entrée non valide")
    rech_note = input("Voulez-vous rechercher une note (oui/non) ?")
while (rech_note == "oui"):
    
    if (len(notes1.note)) != 0:                 # Y a-t-il au moins une note?
        quel_nom = input("Nom de la note recherchée: ")

    # TRAITEMENT D'UNE ERREUR PAR TRY/EXCEPT
        try:
            notes1.afficher(quel_nom)
        except KeyError:
            print("Cette note n'est pas dans le dictionnaire")
        
        rech_note = input("Voulez-vous rechercher une autre note (oui/non) ?")
        while (rech_note == "" or rech_note.isspace()):
            print("Entrée non valide")
            rech_note = input("Voulez-vous rechercher une autre note (oui/non) ?")
    else:
        print("Il n'y a pas encore de note")
        rech_note = "non"
       