### CLASSE BLOC-NOTE

- Créer une classe Note.
- Créer les attributs et les méthodes nécessaires afin que la classe Note permette.
de sauvegarder des notes (str) dans un dictionnaire, consultable via une fonction demandant
la clef correspondant à la valeur que l'utilisateur souhaite afficher.
- Créer un script main.py pour utiliser la classe Note.
- Intégrer les clauses TRY / EXCEPT pour les fonctions d'affichage en cas de mauvaise saisie de la clef 
par l'utilisateur.