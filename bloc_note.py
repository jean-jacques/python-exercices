class Note:
    "Définition d'une classe de notes"

    def __init__(self):
        self.note = {}

    def add(self):                              # Ajout d'une note
        nom = input("Entrez un nom de note: ")
 #       while (not(nom and not nom.isspace())):
 #       while (nom.isspace()):
        while (nom == "" or nom.isspace()):
            print("Le nom ne peut pas être vide")
            nom = input("Entrez un nom de note: ")

        contenu = input("Entrez le texte de la note: ")
        while (contenu == "" or contenu.isspace()):
            print("La note ne peut pas être vide")
            contenu = input("Entrez le texte de la note: ")
              
        self.note[nom] = contenu
        
    def toString(self):
        print("note = " + str(self.note))

    def search(self):                           # Recherche dans les notes
        if len(self.note) != 0:                 # Y a-t-il au moins une note?
            while True:
                try:
                    quel_nom = input("Nom de la note recherchée: ")
                    print(self.note[quel_nom])
                    break
                except:
                    print("Cette note n'est pas dans le dictionnaire")
        else:
            print("Il n'y a pas encore de note")
